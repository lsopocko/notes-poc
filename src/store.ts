import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    connecting: false,
    selectedNote: 0,
    notes: [
      {
        id: 1,
        label: "Note",
        link: 0,
      },
      {
        id: 2,
        label: "Note",
        link: 0,
      },
      {
        id: 3,
        label: "Note",
        link: 0,
      },
    ],
  },
  mutations: {
    SET_CONNECTING_STATE(state, status) {
      state.connecting = status;
    },
    SELECT_NOTE(state, noteId) {
      state.selectedNote = noteId;
    },
    SET_RELATION(state, noteId) {
      for (const noteIndex in state.notes) {
        if (state.notes[noteIndex].id === state.selectedNote) {
          state.notes[noteIndex].link = noteId;
        }
      }
    },
  },
  actions: {
    connectStart: (context, noteId) => {
      context.commit("SET_CONNECTING_STATE", true);
      context.commit("SELECT_NOTE", noteId);
    },
    connectEnd: (context, noteId) => {
      context.commit("SET_CONNECTING_STATE", false);
      context.commit("SET_RELATION", noteId);
      context.commit("SELECT_NOTE", 0);
    },
  },
});
